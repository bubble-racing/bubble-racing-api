package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"

	"github.com/stripe/stripe-go/v75"
	"github.com/stripe/stripe-go/v75/checkout/session"
	"github.com/stripe/stripe-go/v75/webhook"
)

func handleStripeCreateCheckoutSession(w http.ResponseWriter, r *http.Request) {
	priceIdStr := r.URL.Query().Get("priceId")
	priceId, err := strconv.ParseInt(priceIdStr, 10, 64)
	if err != nil {
		http.Error(w, fmt.Sprintf("error parsing quantity %v", err.Error()), http.StatusInternalServerError)
		return
	}
	domainURL := os.Getenv("DOMAIN")
	priceEnvVarName := fmt.Sprintf("STRIPE_BUBBLE_PRICE_%v", priceId)
	bubbleBytes, _ := json.Marshal(GenerateCurrentFeaturedBubble())

	// ?session_id={CHECKOUT_SESSION_ID} means the redirect will have the session ID
	// set as a query param
	params := &stripe.CheckoutSessionParams{
		SuccessURL: stripe.String(domainURL + "/api/stripe/checkout-complete?stripe_session_id={CHECKOUT_SESSION_ID}"),
		CancelURL:  stripe.String(domainURL + "/"),
		Mode:       stripe.String(string(stripe.CheckoutSessionModePayment)),
		Metadata: map[string]string{
			"bubble": string(bubbleBytes),
		},
		LineItems: []*stripe.CheckoutSessionLineItemParams{
			{
				Quantity: stripe.Int64(1),
				Price:    stripe.String(os.Getenv(priceEnvVarName)),
			},
		},
	}
	s, err := session.New(params)
	if err != nil {
		http.Error(w, fmt.Sprintf("error while creating session %v", err.Error()), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, s.URL, http.StatusSeeOther)
}

func handleStripeWebhook(w http.ResponseWriter, r *http.Request) {
	b, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Printf("io.ReadAll: %v", err)
		return
	}

	event, err := webhook.ConstructEvent(b, r.Header.Get("Stripe-Signature"), os.Getenv("STRIPE_WEBHOOK_SECRET"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		log.Printf("webhook.ConstructEvent: %v", err)
		return
	}

	if event.Type == "checkout.session.completed" {
		fmt.Println("Checkout Session completed!")
	}

	respondWithJSON(w, http.StatusOK, nil)
}

func handleStripeCheckoutComplete(w http.ResponseWriter, r *http.Request) {
	sessionID := r.URL.Query().Get("stripe_session_id")
	sess, _ := session.Get(sessionID, nil)
	url := os.Getenv("DOMAIN")
	bubbleStr, _ := sess.Metadata["bubble"]
	responseStr := fmt.Sprintf(`
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>Stripe Redirect</title>
		</head>
		<body>
			<h1>Hello! Your purchase is completed!</h1>
			<p>If you don't redirect automatically <a href="%v">click here</a>.</p>
			<script>
				function saveBubbleAndRedirect(bubbleStr, redirectUrl) {
					var dbName = "/userfs";
					var storeName = "FILE_DATA";
					var fileName = "/userfs/bubbleracing.save"
					var request = indexedDB.open(dbName);
					var bubble = JSON.parse(bubbleStr)
					
					request.onupgradeneeded = function(event) {
						var db = event.target.result;
						if(!db.objectStoreNames.contains(storeName)) {
							db.createObjectStore(storeName, { keyPath: "" });
						}
					};
					
					request.onerror = function(event) {
						console.error("DB error: ", event, event.target.error ? event.target.error : 'unknown error');
					};
					
					request.onsuccess = function(event) {
						var db = event.target.result;
						var transaction = db.transaction([storeName], "readwrite");
						var objectStore = transaction.objectStore(storeName);
						
						// Try to get an existing object
						var getRequest = objectStore.get(fileName);
						
						getRequest.onsuccess = function(event) {
							var result = event.target.result;
							var decoder = new TextDecoder('utf-8');
							
							if (!result) {
								console.log('error, no save data found')
								return
							}
		
							// If object found, decode and merge data
							console.log("Data found, merging with new data");
							var saveStr = decoder.decode(result.contents);
							var saveObj = JSON.parse(saveStr)
							saveObj.bubbles[bubble.id] = bubble
							saveObj.current_bubble_id = bubble.id
							saveObj.next_load_scene = "bubble-picker"
							saveStr = JSON.stringify(saveObj)
							result.contents = new TextEncoder().encode(saveStr)
							
							// Save data
							var putRequest = objectStore.put(result, fileName);
							
							putRequest.onsuccess = function(event) {
								console.log("Data written successfully!");
								window.location.href = redirectUrl; // Redirecting to new page
							};
							
							putRequest.onerror = function(event) {
								console.log("Error writing data: ", event.target.errorCode);
							};
						};
						
						getRequest.onerror = function(event) {
							console.log("Error reading data: ", event.target.errorCode);
						};
					};
				}
		
				saveBubbleAndRedirect('%v', "%v")
			</script>
		</body>
		</html>
	`, url, bubbleStr, url)

	w.Header().Set("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(responseStr))
}
