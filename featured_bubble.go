package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"
)

// Stats represents an object with random values for 'speed', 'tough', and 'skill'.
type Stats struct {
	Speed int `json:"speed"`
	Tough int `json:"tough"`
	Skill int `json:"skill"`
}

type BubblePart struct {
	Id    int    `json:"id"`
	Color string `json:"color"`
}

// Bubble config
type Bubble struct {
	ID      string     `json:"id"`
	Nature  Stats      `json:"nature"`
	Nurture Stats      `json:"nurture"`
	Name    string     `json:"name"`
	Base    BubblePart `json:"base"`
	Eyes    BubblePart `json:"eyes"`
	Pupils  BubblePart `json:"pupils"`
	Mouth   BubblePart `json:"mouth"`
	Ear     BubblePart `json:"ear"`
	Hat     BubblePart `json:"hat"`
}

type FeaturedBubbleResponse struct {
	Bubble   Bubble `json:"bubble"`
	Note     string `json:"note"`
	Discount int    `json:"discount"`
	TimeLeft int    `json:"timeLeft"`
}

var firstNames = []string{
	"Bella",
	"Max",
	"Olivia",
	"Liam",
	"Ava",
	"Mason",
	"Sophia",
	"Jackson",
	"Emma",
	"Ethan",
	"Grace",
	"Noah",
	"Aiden",
	"Mia",
	"Lucas",
	"Isabella",
	"Owen",
	"Madison",
	"Jameson",
	"Scarlett",
	"Carter",
	"Amelia",
	"Elijah",
	"Chloe",
	"Caleb",
	"Harper",
	"Benjamin",
	"Addison",
	"William",
	"Zoey",
	"Samuel",
	"Penelope",
	"Grayson",
	"Abigail",
	"Daniel",
	"Christopher",
	"Avery",
	"Henry",
	"Victoria",
	"Sebastian",
	"Elizabeth",
	"Logan",
	"Ella",
	"Nathan",
	"Lily",
	"Christian",
	"Zoe",
}

var lastNames = []string{
	"Chase",
	"Monroe",
	"Steele",
	"Harper",
	"Falcon",
	"West",
	"Blaze",
	"Storm",
	"Ryder",
	"Blackwell",
	"Rivers",
	"Fox",
	"Silver",
	"Stone",
	"Vega",
	"Gold",
	"Blake",
	"Cruz",
	"Star",
	"Orion",
	"Celeste",
	"Nova",
	"Moon",
	"Hunter",
	"Phoenix",
	"Archer",
	"Wilde",
	"Sterling",
	"Legend",
	"Jet",
	"Crimson",
	"Knight",
	"Valencia",
	"Hawk",
	"Raven",
}

// GenerateRandomColor generates a random color within the specified brightness range
// and returns it as an HTML color string (#RRGGBB).
func generateRandomColor(rng *rand.Rand, minBrightness, maxBrightness float64) string {
	if minBrightness < 0 {
		minBrightness = 0
	}
	if maxBrightness > 1 {
		maxBrightness = 1
	}

	// Calculate the minimum and maximum brightness in the RGB color space (0-255).
	minRGB := int(minBrightness * 255)
	maxRGB := int(maxBrightness * 255)

	// Generate random RGB values within the specified range.
	red := rng.Intn(maxRGB-minRGB+1) + minRGB
	green := rng.Intn(maxRGB-minRGB+1) + minRGB
	blue := rng.Intn(maxRGB-minRGB+1) + minRGB

	// Format the RGB values as a hexadecimal HTML color string.
	colorString := fmt.Sprintf("#%02X%02X%02X", red, green, blue)

	return colorString
}

func generateRandomFullName(rng *rand.Rand, firstNames, lastNames []string) string {
	if len(firstNames) == 0 || len(lastNames) == 0 {
		return "No names provided"
	}

	randomFirstName := firstNames[rng.Intn(len(firstNames))]
	randomLastName := lastNames[rng.Intn(len(lastNames))]

	return randomFirstName + " " + randomLastName
}

func GenerateCurrentFeaturedBubble() Bubble {
	currentTime := time.Now().Unix()
	hoursSinceEpoch := currentTime / 3600
	bubble := GenerateFeaturedBubble(hoursSinceEpoch)
	return bubble
}

func GenerateFeaturedBubble(seed int64) Bubble {
	rng := rand.New(rand.NewSource(seed))

	return Bubble{
		ID:   fmt.Sprint(seed),
		Name: generateRandomFullName(rng, firstNames, lastNames),
		Nature: Stats{
			Speed: rng.Intn(4),
			Tough: rng.Intn(4),
			Skill: rng.Intn(4),
		},
		Nurture: Stats{
			Speed: 0,
			Tough: 0,
			Skill: 0,
		},
		Base: BubblePart{
			Id:    101 + rng.Intn(2),
			Color: generateRandomColor(rng, 0.2, 1.0),
		},
		Eyes: BubblePart{
			Id:    101 + rng.Intn(2),
			Color: generateRandomColor(rng, 0.85, 1.0),
		},
		Pupils: BubblePart{
			Id:    101 + rng.Intn(2),
			Color: generateRandomColor(rng, 0.0, 0.85),
		},
		Mouth: BubblePart{
			Id:    101 + rng.Intn(2),
			Color: generateRandomColor(rng, 0.3, 1.0),
		},
		Ear: BubblePart{
			Id:    101 + rng.Intn(2),
			Color: generateRandomColor(rng, 0.5, 1.0),
		},
		Hat: BubblePart{
			Id:    101 + rng.Intn(2),
			Color: generateRandomColor(rng, 0.0, 1.0),
		},
	}
}

func (a *App) getFeaturedBubbleHandler(w http.ResponseWriter, r *http.Request) {
	// Get results based on hours since epoch
	currentTime := time.Now().Unix()
	hoursSinceEpoch := currentTime / 3600
	bubble := GenerateFeaturedBubble(hoursSinceEpoch)
	response := FeaturedBubbleResponse{
		Bubble:   bubble,
		Note:     "",
		Discount: 0,
		TimeLeft: int(3600 - (currentTime % 3600)),
	}
	respondWithJSON(w, http.StatusOK, response)
}
