package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"strconv"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestHandleLevelPost(t *testing.T) {
	level := LevelPostBody{
		Nickname: "test",
		Site:     "test",
		Title:    "test",
		Map:      Map{"0": Column{"0": 1}},
	}

	jsonLevel, _ := json.Marshal(level)

	req, _ := http.NewRequest("POST", "/levels", bytes.NewBuffer(jsonLevel))
	response := executeRequest(req)

	assert.Equal(t, 201, response.Code, "Expected response code 201")

	var responseLevel Level
	_ = json.Unmarshal(response.Body.Bytes(), &responseLevel)
	assert.Equal(t, level.Nickname, responseLevel.Nickname, "Expected the same nickname")
}

func TestHandleLevelGet(t *testing.T) {
	level := &Level{
		Nickname:     "test",
		Site:         "test",
		UpdatedAt:    time.Now(),
		Title:        "test",
		Map:          Map{"0": Column{"0": 1}},
		Status:       Pending,
		VotesFor:     0,
		VotesAgainst: 0,
	}

	_ = a.db.Create(level)

	req, _ := http.NewRequest("GET", "/levels/"+strconv.Itoa(level.ID), nil)
	response := executeRequest(req)

	assert.Equal(t, 200, response.Code, "Expected response code 200")

	var responseLevel Level
	_ = json.Unmarshal(response.Body.Bytes(), &responseLevel)
	assert.Equal(t, level.ID, responseLevel.ID, "Expected the same level ID")
}

func TestHandleLevelListGet(t *testing.T) {
	// Create a couple of levels
	level1 := &Level{
		Nickname:     "test1",
		Site:         "test1",
		UpdatedAt:    time.Now(),
		Title:        "test1",
		Map:          Map{"0": Column{"0": 1}},
		Status:       Pending,
		VotesFor:     0,
		VotesAgainst: 0,
	}
	level2 := &Level{
		Nickname:     "test1",
		Site:         "test1",
		UpdatedAt:    time.Now(),
		Title:        "test2",
		Map:          Map{"0": Column{"0": 2}},
		Status:       Pending,
		VotesFor:     0,
		VotesAgainst: 0,
	}

	_ = a.db.Create(level1)
	_ = a.db.Create(level2)

	req, _ := http.NewRequest("GET", "/levels?nickname=test1&site=test1", nil)
	response := executeRequest(req)

	assert.Equal(t, 200, response.Code, "Expected response code 200")

	var responseLevels []Level
	_ = json.Unmarshal(response.Body.Bytes(), &responseLevels)
	assert.Equal(t, 2, len(responseLevels), "Expected to get 2 levels")
}

func TestHandleLevelGetNotFound(t *testing.T) {
	req, _ := http.NewRequest("GET", "/levels/9999", nil)
	response := executeRequest(req)

	assert.Equal(t, 404, response.Code, "Expected response code 404")
}
