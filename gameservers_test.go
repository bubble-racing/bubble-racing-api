package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

// there are no gameservers

func TestGetLobbyNoneAvailable(t *testing.T) {
	req, _ := http.NewRequest("GET", "/lobby", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusInternalServerError, response.Code)
}

// pass a wrong api key
func TestBadKey(t *testing.T) {

	var createdAt = "2021-09-14T16:21:11-04:00"
	var jsonStr = []byte(fmt.Sprintf(`{"gameserver_id":5, "address":"localhost", "port":5555, "player_count": 0, "created_at": "%s", "updated_at": "%s"}`, createdAt, createdAt))
	req, _ := http.NewRequest("PUT", "/gameservers/3", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Gameserver-Key", "wrong-key")

	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
}

func TestCreateGameserverViaPut(t *testing.T) {

	var createdAt = "2021-09-14T16:21:11-04:00"
	var jsonStr = []byte(fmt.Sprintf(`{"address":"localhost", "port":5555, "player_count": 0, "is_open": true, "created_at": "%s", "updated_at": "%s"}`, createdAt, createdAt))
	req, _ := http.NewRequest("PUT", "/gameservers/7", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Gameserver-Key", "test-gameserver-key")

	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)

	if m["address"] != "localhost" {
		t.Errorf("Expected gameserver address to be 'localhost'. Got '%v'", m["name"])
	}

	if m["port"] != 5555.0 {
		t.Errorf("Expected gameserver port to be 5555. Got '%v'", m["port"])
	}

	if m["player_count"] != 0.0 {
		t.Errorf("Expected gameserver player_count to be 0. Got '%v'", m["player_count"])
	}

	if m["is_open"] != true {
		t.Errorf("Expected is_open to be true. Got '%v'", m["is_open"])
	}

	// the id is compared to 1.0 because JSON unmarshaling converts numbers to
	// floats, when the target is a map[string]interface{}
	if m["gameserver_id"] != 7.0 {
		t.Errorf("Expected gameserver ID to be '7'. Got '%v'", m["gameserver_id"])
	}
}

func TestUpdateGameserverViaPut(t *testing.T) {

	var createdAt = "2000-09-14T16:21:11-04:00"
	var jsonStr = []byte(fmt.Sprintf(`{"address":"127.0.0.1", "port":4444, "player_count": 3, "created_at": "%s"}`, createdAt))
	req, _ := http.NewRequest("PUT", "/gameservers/7", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Gameserver-Key", "test-gameserver-key")

	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)

	if m["gameserver_id"] != 7.0 {
		t.Errorf("Expected the gameserver_id to remain the same (%v). Got %v", 7, m["gameserver_id"])
	}

	if m["address"] != "127.0.0.1" {
		t.Errorf("Expected gameserver address to be '127.0.0.1'. Got '%v'", m["name"])
	}

	if m["port"] != 4444.0 {
		t.Errorf("Expected gameserver port to be 5555. Got '%v'", m["port"])
	}

	if m["player_count"] != 3.0 {
		t.Errorf("Expected gameserver player_count to be 0. Got '%v'", m["player_count"])
	}
}

// an existing gameserver should be joined

func TestGetLobbyExisting(t *testing.T) {
	req, _ := http.NewRequest("GET", "/lobby", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)

	if m["gameserver_id"] != 7.0 {
		t.Errorf("Expected gameserver_id to equal (7). Got %v", m["gameserver_id"])
	}
	if m["address"] != "127.0.0.1" {
		t.Errorf("Expected address to equal (127.0.0.1). Got %v", m["address"])
	}
	if m["player_count"] != 4.0 {
		t.Errorf("Expected player_count to equal (4). Got %v", m["player_count"])
	}
}

// there are gameservers, but they are all full

func TestGetLobbyAllFull(t *testing.T) {
	req, _ := http.NewRequest("GET", "/lobby", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusInternalServerError, response.Code)
}
