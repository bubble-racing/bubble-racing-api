package main

import (
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func TestHandleStripeCreateCheckoutSession(t *testing.T) {
	if os.Getenv("STRIPE_SECRET_KEY") == "" {
		t.Skip("Skipping stripe test, api keys are not set")
	}

	// Create a mock HTTP request with form values
	req, err := http.NewRequest("GET", "/stripe/create-checkout-session?priceId=1", nil)
	if err != nil {
		t.Fatal(err)
	}

	// Create a response recorder to record the response
	rr := httptest.NewRecorder()

	// Call the handler function
	handleStripeCreateCheckoutSession(rr, req)

	// Check the HTTP status code
	if rr.Code != http.StatusSeeOther {
		t.Errorf("Expected status code %d, got %d. Response Body: %s", http.StatusSeeOther, rr.Code, rr.Body.String())
	}

	// Check if the Location header is set to the expected URL
	expectedURL := "https://checkout.stripe.com/"
	if !strings.HasPrefix(rr.Header().Get("Location"), expectedURL) {
		t.Errorf("Expected Location header to start with %s, got %s", expectedURL, rr.Header().Get("Location"))
	}
}

func TestHandleStripeWebhook(t *testing.T) {
	// Create a mock HTTP request with a sample webhook payload
	payload := `{"id":"evt_testevent","type":"checkout.session.completed"}`
	req, err := http.NewRequest("POST", "/stripe/webhook", strings.NewReader(payload))
	if err != nil {
		t.Fatal(err)
	}

	// Set the Stripe-Signature header to a valid signature for the payload
	req.Header.Set("Stripe-Signature", "valid_signature")

	// Create a response recorder to record the response
	rr := httptest.NewRecorder()

	// Call the handler function
	handleStripeWebhook(rr, req)

	// Check the HTTP status code, will be 400 because I don't know how to create a valid Stripe-Signature header
	if rr.Code != http.StatusBadRequest {
		t.Errorf("Expected status code %d, got %d", http.StatusBadRequest, rr.Code)
	}
}
