package main

import "net/http"

type healthResponse struct {
	healthy bool
}

func (a *App) getHealth(w http.ResponseWriter, r *http.Request) {
	response := healthResponse{healthy: true}
	respondWithJSON(w, http.StatusOK, response)
}
