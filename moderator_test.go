package main

import (
	"image/color"
	"image/png"
	"os"
	"testing"
)

func TestRenderToPng(t *testing.T) {
	data := [][]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}
	filename := "test.png"

	err := renderToPng(data, filename)
	if err != nil {
		t.Errorf("Error rendering PNG: %v", err)
	}

	// Open the generated PNG file
	f, err := os.Open(filename)
	if err != nil {
		t.Errorf("Error opening PNG file: %v", err)
	}
	defer f.Close()

	// Decode the PNG image
	img, err := png.Decode(f)
	if err != nil {
		t.Errorf("Error decoding PNG image: %v", err)
	}

	// Verify the dimensions of the image
	expectedWidth := len(data[0]) * 5
	expectedHeight := len(data) * 5
	if img.Bounds().Dx() != expectedWidth || img.Bounds().Dy() != expectedHeight {
		t.Errorf("Unexpected image dimensions. Expected %dx%d, got %dx%d",
			expectedWidth, expectedHeight, img.Bounds().Dx(), img.Bounds().Dy())
	}

	// Verify the colors of the pixels
	for y, row := range data {
		for x, cell := range row {
			for i := 0; i < 5; i++ {
				for j := 0; j < 5; j++ {
					expectedColor := colors[cell]
					actualColor := img.At(x*5+i, y*5+j)
					if !colorsEqual(expectedColor, actualColor) {
						t.Errorf("Unexpected color at (%d, %d). Expected %v, got %v",
							x*5+i, y*5+j, expectedColor, actualColor)
					}
				}
			}
		}
	}

	// Clean up the generated PNG file
	err = os.Remove(filename)
	if err != nil {
		t.Errorf("Error removing PNG file: %v", err)
	}
}

// Helper function to compare colors
func colorsEqual(c1, c2 color.Color) bool {
	r1, g1, b1, a1 := c1.RGBA()
	r2, g2, b2, a2 := c2.RGBA()
	return r1 == r2 && g1 == g2 && b1 == b2 && a1 == a2
}
