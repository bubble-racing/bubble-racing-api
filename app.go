package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/stripe/stripe-go/v75"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	_ "modernc.org/sqlite"
)

type App struct {
	Router         *mux.Router
	gameserver_key string
	gameservers    map[int]Gameserver
	db             *gorm.DB
}

func (a *App) Initialize() {
	// set up sqlite
	db, err := gorm.Open(sqlite.Open(os.Getenv("SQLITE_FILE")), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	a.db = db

	// Migrate the schema
	db.AutoMigrate(&Level{})

	// set up routes
	a.Router = mux.NewRouter()
	a.initializeRoutes()
	log.Println("Initialize complete")

	// keep gameserver_key to verify api calls from gameservers
	a.gameserver_key = os.Getenv("GAMESERVER_KEY")
	if a.gameserver_key == "" {
		a.gameserver_key = "test-gameserver-key"
	}

	// start with an empty list of gameservers
	a.gameservers = map[int]Gameserver{}

	// set up stripe
	stripe.Key = os.Getenv("STRIPE_SECRET_KEY")
}

func (a *App) Run(addr string) {
	log.Println("Bubble Racing API is listening on port 8080")
	log.Fatal(http.ListenAndServe(":8080", a.Router))
}

func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/health", a.getHealth).Methods("GET")
	a.Router.HandleFunc("/lobby", a.getLobby).Methods("GET")
	a.Router.HandleFunc("/gameservers/{gameserver_id:[0-9]+}", a.updateGameserver).Methods("PUT")
	a.Router.HandleFunc("/featured-bubble", a.getFeaturedBubbleHandler).Methods("GET")
	a.Router.HandleFunc("/stripe/create-checkout-session", handleStripeCreateCheckoutSession).Methods("GET")
	a.Router.HandleFunc("/stripe/webhook", handleStripeWebhook).Methods("POST")
	a.Router.HandleFunc("/stripe/checkout-complete", handleStripeCheckoutComplete).Methods("GET")
	a.setupLevelRoutes()
}

func respondWithError(w http.ResponseWriter, code int, message string) {
	log.Println(message)
	respondWithJSON(w, code, map[string]string{"error": message})
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
