package main

import (
	"testing"
)

func TestGenerateFeaturedBubble(t *testing.T) {
	bubble := GenerateFeaturedBubble(0)

	t.Run("Test Nature and Nurture stats", func(t *testing.T) {
		validateStats(t, bubble.Nature, "Nature")
		validateStats(t, bubble.Nurture, "Nurture")
	})

	t.Run("Test Name", func(t *testing.T) {
		if len(bubble.Name) < 4 {
			t.Errorf("Invalid Name: %s", bubble.Name)
		}
	})

	t.Run("Test BubblePart properties", func(t *testing.T) {
		validateBubblePart(t, bubble.Base)
		validateBubblePart(t, bubble.Eyes)
		validateBubblePart(t, bubble.Pupils)
		validateBubblePart(t, bubble.Mouth)
		validateBubblePart(t, bubble.Ear)
		validateBubblePart(t, bubble.Hat)
	})
}

func validateStats(t *testing.T, stats Stats, label string) {
	t.Helper()

	if stats.Speed < 0 || stats.Speed > 3 {
		t.Errorf("Invalid %s.Speed: %d", label, stats.Speed)
	}
	if stats.Tough < 0 || stats.Tough > 3 {
		t.Errorf("Invalid %s.Tough: %d", label, stats.Tough)
	}
	if stats.Skill < 0 || stats.Skill > 3 {
		t.Errorf("Invalid %s.Skill: %d", label, stats.Skill)
	}
}

func validateBubblePart(t *testing.T, part BubblePart) {
	if part.Id < 100 || part.Id > 103 {
		t.Errorf("Invalid BubblePart Id: %d", part.Id)
	}
	if len(part.Color) != 7 || part.Color[0] != '#' {
		t.Errorf("Invalid BubblePart Color: %s", part.Color)
	}
}
