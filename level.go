package main

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

type Status int

const (
	Unpublished Status = iota
	Pending
	Published
	Rejected
)

func (s Status) String() string {
	return [...]string{"unpublished", "pending", "published", "rejected"}[s]
}

type Column map[string]int
type Map map[string]Column

type Level struct {
	gorm.Model
	ID           int       `json:"id" gorm:"primaryKey"`
	Nickname     string    `json:"nickname"`
	Site         string    `json:"site"`
	UpdatedAt    time.Time `json:"updated_at"`
	Title        string    `json:"title"`
	VotesFor     int       `json:"votes_for"`
	VotesAgainst int       `json:"votes_against"`
	Map          Map       `json:"map" gorm:"type:text"`
	Status       Status    `json:"status"`
}

type LevelListItem struct {
	ID       int    `json:"id"`
	Title    string `json:"title"`
	Status   Status `json:"status"`
	Nickname string `json:"nickname"`
}

type LevelPostBody struct {
	Nickname string `json:"nickname"`
	Site     string `json:"Site"`
	Title    string `json:"title"`
	Map      Map    `json:"map"`
}

// Value makes the Map struct implement the driver.Valuer interface.
// This method simply returns the JSON-encoded representation of the struct.
func (m Map) Value() (driver.Value, error) {
	return json.Marshal(m)
}

// Scan makes the Map struct implement the sql.Scanner interface.
// This method takes the JSON-encoded data from the DB and unmarshals it
// to the struct fields.
func (m *Map) Scan(value interface{}) error {
	bytes, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	if err := json.Unmarshal(bytes, &m); err != nil {
		return err
	}

	return nil
}

func (a *App) setupLevelRoutes() {
	a.Router.HandleFunc("/levels", a.handleLevelPost).Methods("POST")
	a.Router.HandleFunc("/levels", a.handleLevelListGet).Methods("GET")
	a.Router.HandleFunc("/levels/{level_id:[0-9]+}", a.handleLevelGet).Methods("GET")
}

func (a *App) getLevel(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	level_id, err := strconv.Atoi(vars["level_id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid level ID")
		return
	}

	var level Level
	result := a.db.First(&level, level_id)
	if result.Error != nil {
		respondWithError(w, http.StatusNotFound, "Level not found")
		return
	}

	respondWithJSON(w, http.StatusOK, level)
}

func (a *App) handleLevelPost(w http.ResponseWriter, r *http.Request) {
	var level Level
	var body LevelPostBody
	db := a.db

	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		log.Printf("Error decoding JSON: %v", err)
		respondWithError(w, http.StatusBadRequest, "Could not decode json body")
		return
	}

	// see if level title already exists
	result := db.Where(&Level{Nickname: body.Nickname, Site: body.Site, Title: body.Title}).First(&level)

	// if it does not, create new level
	if errors.Is(result.Error, gorm.ErrRecordNotFound) {
		level = Level{
			Nickname:     body.Nickname,
			Site:         body.Site,
			UpdatedAt:    time.Now(),
			Title:        body.Title,
			Map:          body.Map,
			Status:       Pending,
			VotesFor:     0,
			VotesAgainst: 0,
		}
		result := db.Create(&level)
		if result.Error != nil {
			log.Printf("Error creating level: %v", result.Error)
			respondWithError(w, http.StatusInternalServerError, "Could not create level")
			return
		}
		respondWithJSON(w, http.StatusCreated, level)
	} else {
		result := db.Model(&level).Updates(Level{
			UpdatedAt: time.Now(),
			Map:       body.Map,
			Status:    Pending,
		})
		if result.Error != nil {
			log.Printf("Error updating level: %v", result.Error)
			respondWithError(w, http.StatusInternalServerError, "Could not update level")
			return
		}
		respondWithJSON(w, http.StatusOK, level)
	}
}

func (a *App) handleLevelGet(w http.ResponseWriter, r *http.Request) {
	var level Level
	db := a.db

	vars := mux.Vars(r)
	level_id, err := strconv.Atoi(vars["level_id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid level_id")
		return
	}

	result := db.First(&level, level_id)
	if result.Error != nil {
		respondWithError(w, http.StatusNotFound, "Level not found")
		return
	}

	respondWithJSON(w, http.StatusOK, level)
}

func (a *App) handleLevelListGet(w http.ResponseWriter, r *http.Request) {
	var levels []Level
	nickname := r.URL.Query().Get("nickname")
	site := r.URL.Query().Get("site")

	result := a.db.Where("Nickname = ? AND Site = ?", nickname, site).Find(&levels)
	if result.Error != nil {
		log.Printf("Error fetching levels: %v", result.Error)
		respondWithError(w, http.StatusInternalServerError, "Could not fetch levels")
		return
	}

	var levelListItems []LevelListItem
	for _, level := range levels {
		levelListItems = append(levelListItems, LevelListItem{
			ID:       level.ID,
			Nickname: level.Nickname,
			Title:    level.Title,
			Status:   level.Status,
		})
	}

	respondWithJSON(w, http.StatusOK, levelListItems)
}
