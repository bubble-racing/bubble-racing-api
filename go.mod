module bubble-racing-api

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.10.4
	github.com/stretchr/testify v1.7.0
	github.com/stripe/stripe-go/v75 v75.10.0
	golang.org/x/net v0.1.0 // indirect
	gorm.io/driver/sqlite v1.5.4
	gorm.io/gorm v1.25.5
	modernc.org/sqlite v1.28.0
)
