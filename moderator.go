package main

import (
	"image"
	"image/color"
	"image/png"
	"os"
)

const EMPTY = 0
const WALL = 1
const PHOSPHORUS = 2
const SPEED = 3
const CHEST = 4
const DEBRIS = 5
const VORTEX = 6
const GHOST = 7
const OIL = 8
const BUBBLE = 9
const BREADCRUMBS = 10
const KEY = 11
const CURRENT_UP = 12
const CURRENT_RIGHT = 13
const CURRENT_DOWN = 14
const CURRENT_LEFT = 15
const TURTLE = 16
const DIVER = 17
const CHERRIES = 18
const STARFISH = 19

var colors = map[int]color.RGBA{
	EMPTY:         {255, 255, 255, 255},
	WALL:          {0, 0, 0, 255},
	PHOSPHORUS:    {255, 0, 0, 255},
	SPEED:         {0, 255, 0, 255},
	CHEST:         {0, 0, 255, 255},
	DEBRIS:        {255, 255, 0, 255},
	VORTEX:        {255, 0, 255, 255},
	GHOST:         {0, 255, 255, 255},
	OIL:           {255, 128, 0, 255},
	BUBBLE:        {128, 0, 255, 255},
	BREADCRUMBS:   {0, 255, 128, 255},
	KEY:           {255, 0, 128, 255},
	CURRENT_UP:    {128, 255, 0, 255},
	CURRENT_RIGHT: {0, 128, 255, 255},
	CURRENT_DOWN:  {255, 0, 128, 255},
	CURRENT_LEFT:  {0, 255, 128, 255},
	TURTLE:        {128, 128, 128, 255},
	DIVER:         {128, 128, 128, 255},
	CHERRIES:      {255, 128, 128, 255},
	STARFISH:      {128, 255, 128, 255},
}

func renderToPng(data [][]int, filename string) error {
	// Define the size of the image (change as needed)
	const W, H = 5, 5
	img := image.NewRGBA(image.Rect(0, 0, W*len(data[0]), H*len(data)))

	// Iterate over the data and set pixels
	for y, row := range data {
		for x, cell := range row {
			for i := 0; i < W; i++ {
				for j := 0; j < H; j++ {
					color := colors[cell]
					img.Set(x*W+i, y*H+j, color)
				}
			}
		}
	}

	// Create a file
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()

	// Encode as a PNG
	return png.Encode(f, img)
}
