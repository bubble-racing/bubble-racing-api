## Testing

### Prerequesites
* go

### Lint
```
gofmt -s -w .
```

### Test command
```
go test -v
```