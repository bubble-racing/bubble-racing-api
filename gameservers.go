package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
)

type Gameserver struct {
	GameserverID int       `json:"gameserver_id"`
	Address      string    `json:"address"`
	Port         int       `json:"port"`
	PlayerCount  int       `json:"player_count"`
	IsOpen       bool      `json:"is_open"`
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

func (a *App) getLobby(w http.ResponseWriter, r *http.Request) {
	// get list of open gameservers
	openGameservers := []Gameserver{}
	log.Println("Looking for open gameservers")
	for _, gameserver := range a.gameservers {
		log.Println("Found a gameserver")
		if gameserver.PlayerCount < 4 {
			openGameservers = append(openGameservers, gameserver)
		}
	}

	// if none are found, return error
	if len(openGameservers) <= 0 {
		respondWithError(w, http.StatusInternalServerError, "There are no available gameservers, try again")
		return
	}

	// pick the first gameserver
	gameserver := openGameservers[0]

	// increment player count
	gameserver.PlayerCount += 1

	// persist update
	a.gameservers[gameserver.GameserverID] = gameserver

	// send gameserver info to player
	respondWithJSON(w, http.StatusOK, gameserver)
}

func (a *App) updateGameserver(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	log.Println("update Gameserver " + vars["gameserver_id"])

	gameserver_id, err := strconv.Atoi(vars["gameserver_id"])
	if err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid gameserver ID")
		return
	}

	// check gameserver key
	if a.gameserver_key != r.Header.Get("Gameserver-Key") {
		respondWithError(w, http.StatusBadRequest, "Invalid gameserver key")
		return
	}

	var gameserver *Gameserver
	for _, existingGameserver := range a.gameservers {
		if existingGameserver.GameserverID == gameserver_id {
			gameserver = &existingGameserver
			break
		}
	}

	if gameserver == nil {
		gameserver = &Gameserver{
			GameserverID: gameserver_id,
			CreatedAt:    time.Now(),
		}
	}

	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(gameserver); err != nil {
		respondWithError(w, http.StatusBadRequest, "Invalid resquest payload: "+err.Error())
		return
	}
	defer r.Body.Close()

	gameserver.UpdatedAt = time.Now()

	// persist changes to gameserver
	a.gameservers[gameserver_id] = *gameserver

	respondWithJSON(w, http.StatusOK, gameserver)
}
